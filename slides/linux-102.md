---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->
![bg left:30% 85% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/linux.png)


# Linux 102

### Administration Linux

-emmanuel.braux@imt-atlantique.fr-

---
# Licence informations


Auteur : -emmanuel.braux@imt-atlantique.fr-

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:25% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/linux.png)

# Linux 102

- **Devenir Administrateur**
- **Gérer les Applications**
- **Gérer les Utilisateurs**
- **Gérer les Services**
- **Suivre les performances**

---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:25% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/linux.png)

# Linux 102

- ## > Devenir Administrateur
- *Gérer les Applications*
- *Gérer les Utilisateurs*
- *Gérer les Services*
- *Suivre les performances*

---
# Devenir Administrateur

Commande `sudo` : **S**uper **U**ser **DO**
- Lancer des commandes avec les droits d'un autre utilisateur.
- Principalement utilisée pour l'administration : utilisateur `root`
- Périmètre configurable :
  - Uniquement certaines commandes
  - Droits complets : ouverture d'un shell en tant que root
- `$ sudo ls -l /var/log`


---
# Comment utiliser sudo

Bonnes pratiques pour limiter les risques de compromission, et améliorer la traçabilité.

- Utilisateur root : cas particuliers, et exceptionnels.
  - Se connecter en tant que root : à proscrire
- Utilisateurs possédant des droits limités + `sudo`
  - Utiliser sudo pour "passer root" : à éviter
  - **Utiliser sudo pour les commande qui le nécéssitent : recommandé**
  

---
# Utilisation de sudo

- Executer une commande : `$ sudo passwd lisa`
- Ouvrir un shell `$ sudo -i`
- Avec d'autres utilisateurs que root
  - `sudo -u www-data mkdir /var/www/html/site1`
  
---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Utilisation de sudo"

---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:25% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/linux.png)

# Linux 102

- *Devenir Administrateur*
- ## > Gérer les Applications
- *Gérer les Utilisateurs*
- *Gérer les Services*
- *Suivre les performances*

---
# Gérer les applications

Les applications sont disponibles sous forme de paquets

- **Paquet** (ou package) : archive contenant un logiciel compilé
- **Mainteneur**: personne, travaillant ou non pour une distribution, qui compile
des applications et les propose sous forme de paquetage sur par des "repository".
- **Dépendance**: souvent un paquetage dépend d'un autre
- **Repository** (ou dépôt de paquetages): un repository est un espace de stockage
de paquetages sur internet

---
## Commandes disponibles sous les systèmes Debian/Ubuntu

| Commande | Description |
| -------- | ----------- |
| apt-get | outil de haut niveau - historique |
| apt | évolution d'apt-get, plus complète et conviviale |
| aptitude | interface interactive  |
| dpkg | commande de bas niveau  |

---
## Utilisation d'apt

| Commande | Description |
| -------- | ----------- |
| apt **update** | Mettre à jour le catalogue des paquets disponible depuis les dépôts |
| apt **list** | Lister les paquets |
| apt **search** | Rechercher dans le catalogue des paquets |
| apt **install** | Installer un paquet |
| apt **upgrade** | Mettre à jour le système |

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Installation de paquets sur système Debian/Ubuntu avec apt"


---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:25% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/linux.png)

# Linux 102

- *Devenir Administrateur*
- *Gérer les Applications*
- ## > Gérer les Utilisateurs
- *Gérer les Services*
- *Suivre les performances*

---
## Utilisateurs et groupes

- Identifiés par un nom
- Mais surtout un identifiant unique :
  - Utilisateurs : UID (**U**ser **ID**entifier)
  - Groupes : GID (**G**roup **ID**entifier)
- Un groupe primaire, plusieurs groupes secondaires

---
# Identification, Authentification

- Par défaut PAM : (Pluggable Authentication Modules).
- 3 fichiers :
  - `/etc/passwd` : les utilisateurs
  - `/etc/group` : les groupes
  - `/etc/shadow` : les mots de passe chiffrés
- Interconnexion possible avec  LDAP, Active Directory, Kerberos, ...

---
##  Autorisations

- Permissions basées sur les utilisateurs/groupes
- Usage : s'appuyer sur l'appartenance à un groupe

---
## Le compte super-user"

- Nécessaire pour lancer tous les processus de base du système.
- Possède quasiment tous les droits.
- Dans la plupart des cas :
  - Son nom est **root**
  - Son UID est **0**
  - Son prompt est différent des autres utilisateurs, car il contient le symbole **#**

---
## Les commandes

| Commande | Description |
| -------- | ----------- |
| useradd / groupadd | Ajouter un nouvel utilisateur / groupe. |
| userdel / groupdel | Supprimer un utilisateur/groupe |
| usermod / groupmod | Modifier les informations d'un utilisateur / groupe existant. |
| adduser | Gérer l'appartenance à un groupe |
| getent | Interagir avec les fichiers de configuration |
| sudo / su | Agir "en tant que" |


---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Gestion des utilisateurs"

- UID et GID
- Fichiers de configuration
- Gestion des permissions
- Commandes Utilisateur
- Commandes administrateur
- Se connecter "en tant que" avec `su`
- Exécuter des commandes "en tant que" avec `sudo`


---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:25% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/linux.png)

# Linux 102

- *Devenir Administrateur*
- *Gérer les Applications*
- *Gérer les Utilisateurs*
- ## > Gérer les Services
- *Suivre les performances*

---
# Services

- Après le chargement du noyau
- Processus et applications  = **services**
- Dépend du contexte :
  - Serveurs
  - Interface graphique
- Dépendances 

---
# SystemD

![bg left:25% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/systemd.png)

- Apparu en 2010
- Remplacement de init.d
- Désormais généralisé dans quasiment toutes les distributions.
- Simple, performant, souple, puissant, sécurisé
- Nombreuses fonctionnalités avancées : environnement, paramètres, gestion des journaux/logs, sécurité, gestion du redémarrage, ... 

---
#  Element de base : `Unit`

Les principaux types d'Unit :
- `services` : un processus, ou une application.
- `target` : groupe d'unit, correspond à un "état désiré" pour le système.

![h:350 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/systemd-target-service-tree.png)

*Source [https://alperenbayramoglu2.medium.com/systemd-in-nutshell-6c5fd3d5ee52](https://alperenbayramoglu2.medium.com/systemd-in-nutshell-6c5fd3d5ee52)*

--- 
# Autres types d'unit

 - Gestion d’événements et déclenchement de l'exécution d'une unit :
    - A intervalles réguliers : `timer`
    - Au changement d'un fichier ou dossier : `path`
    - A une information reçu via une socket : `socket`
- Gestion de périphériques et du stockage : `device`, `automount`, ...
- Gestion du système :  `snapshot`, ...

---
# Les targets

Les principaux :
| Target   | Description |
| -------- | ----------- |
| **multi-user**.target | Système multi-utilisateur en mode Texte |
| **graphical**.target     | Système multi-utilisateurs en mode graphique  |
| **reboot**.target        | Redémarrer le système. |
| **poweroff**.target      | Éteindre le système. |
| **rescue**.target        | Mode "emergency rescue" (single user) |


---
# Utiliser systemD

Commande de base **`systemctl`**

- Lister les units: `systemctl list-units`
- Activer un service : `systemctl enable myapp.service`
- Démarrer un service : `systemctl start myapp.service`
- Afficher le status : `systemctl status myapp.service `
- Recharcher la configuration  : `systemctl reload myapp.service `
- Lister les targets : `systemctl list-unit-files --type=target`
- Lister les services associés : `systemctl list-dependencies multi-user.target
`
---
# Accéder aux journaux

Commande  `journalctl`  

- Consulter les 100 dernières lignes des journaux, de toutes les unités
```bash
journalctl -n100
```
- Consulter les journaux d'un service
```bash
journalctl -u ssh.service -n50
```
- Consulter les journaux et rester à l'écoute des nouveaux journaux
``` bash
journalctl -u ssh.service -n50 -f
```
---
# Fichiers de configuration

- `/lib/systemd/` : ensemble de fichiers de configuration, organisé par unit.
- `/etc/systemd/` : configurations mises en place pour le système actuel
  - Installées par des paquets
  - Personnalisées
  
Afficher les fichiers de configuration du système `
systemctl list-unit-files`


---
# Syntaxe des fichiers de configuration

- La section **Unit** contient :
    - Des informations sur l'unit : description, ...
    - Les dépendances et conditions pour démarrer l'unit
- La section **Service** décrit la façon de démarrer
    - Comment lancer le service, quel binaire utiliser, quel environnement
    - Comment l'arrêter, que faire cas de d'arrêt inopiné...
    - ...
- La section **Install** elle, décrit quand installer le service.
    - A quel `target` il est associé
---
# Exemple fichier de service

Service simple qui affiche un message dans le journal système :

``` bash
[Unit]
Description=Service de DEMO write-log
After=network.target

[Service]
ExecStart=/usr/local/bin/demo-logs.sh

[Install]
WantedBy=multi-user.target
```
---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Gestion des Services"

- Obtenir des informations sur les Units
- Interagir avec les services
- Interagir avec les targets
- Configurer l'environnement de démarrage par défaut du système : texte ou graphique
- Les fichiers de configuration
- Fichier de configuration d'Unit de type `service`
- Consulter les journaux


---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:25% 70% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/linux.png)

# Linux 102

- *Devenir Administrateur*
- *Gérer les Applications*
- *Gérer les Utilisateurs*
- *Gérer les Services*
- ## > Suivre les performances 

---
# Performance du système

- Gestion du CPU
- Gestion de la mémoire 
- Gestion du swap
- Gestion des IO (disque, ...)
- Gestion des processus

---
# Utilisation de top

Affichage en temps réel  :

- Des informations importantes sur l'état du système
  - État global du système 
  - Aperçu détaillé de l’utilisation des ressources système (CPU, mémoire, ...)
  - État des processus
- De la liste des processus et les ressources qu'ils utilisent.

La commande `top` est très simple à utiliser, disponible sur la plupart des systèmes Unix, et est sûrement la commande la plus efficace pour surveiller les processus.

---
# Exemple de résultat de la commande top :
``` yaml
top - 04:52:40 up 26 min,  1 user,  load average: 0.00, 0.03, 0.16
Tasks: 126 total,   1 running, 125 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.0 us,  0.0 sy,  0.0 ni, 100.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st 
MiB Mem :   1968.2 total,    232.7 free,    347.3 used,   1575.2 buff/cache     
MiB Swap:   2048.0 total,   2048.0 free,      0.0 used.   1621.0 avail Mem 

  PID USER  PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
    1 root      20   0   22560  13696   9600 S   0.0   0.7   0:07.54 systemd
    2 root      20   0       0      0      0 S   0.0   0.0   0:00.00 kthreadd
    3 root      20   0       0      0      0 S   0.0   0.0   0:00.00 pool_workqueue_release
    4 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 kworker/R-rcu_g
```

---
# Informations globales sur le système

``` yaml
top - 04:52:40 up 26 min,  1 user,  load average: 0.00, 0.03, 0.16
```

- **04:52:40**: l'heure actuelle
- **up 26 min** l'uptime de la machine, le temps depuis son dernier démarrage
- **1 user** : le nombre d'utilisateurs connectés
- **load average: 0.00, 0.03, 0.16**: charge moyenne sur les 1, 5 et 15 dernières minutes. 1 = 100 % de charge, 1 par coeur.

---
# Load Average

- Moyenne du nombre de processus en attente d'exécution ou en cours d'exécution sur le CPU :
  - `1.00`: valeur idéale, un processus a toujours accès au CPU sans attente, le processeur est pleinement utilisé sans surcharge.
  - `< 1.00` : le CPU est sous utilisé
  - `> 1.00` : le CPU est dur utilisé, performances dégradées, système potentiellement instable voir inutilisable
- Affichage des 1, 5 et 15 dernières minutes : permet de définir une tendance

- Adapter au nombre de coeurs : 4 coeurs = valeur idéale `4.00`.

---
# Uptime

- Les mise à jour importantes nécessitent le redémarrage du système : uptime de 3 mois, le système est-il à jour ?
- Certains systèmes de fichiers imposent une vérification régulière : 
  - Si nécessaire, cette vérification est lancée au démarrage du système
  - En cas de gros volume de données, un redémarrage peut être très long

---
# Les taches en cours d’exécution

``` yaml
Tasks: 126 total,   1 running, 125 sleeping,   0 stopped,   0 zombie
```

- Nombre total de processus **Tasks: 126 total** 
- Répartition :
  - **1 running** : processus actuellement en cours d'exécution, actifs 
  - **125 sleeping** : processus en sommeil, actifs mais en attente d'un événement.
  - **0 stopped** : processus arrêtés temporairement
  - **0 zombie** : processus en état de "zombie".

---
# Utilisation du CPU

``` yaml
%Cpu(s):  0.0 us,  0.0 sy,  0.0 ni, 100.0 id,  0.0 wa,  0.0 h
```
- `%us` : Temps CPU utilisé par les processus utilisateur, humain ou services
- `%sy` : Temps CPU utilisé par les processus système, noyau.
- `%ni` : Temps CPU utilisé par les processus avec une priorité modifiée (nice).
- `%id` : Temps CPU inactif, ne faisant rien (idle).
- `%wa` : Temps passé à attendre des entrées/sorties (I/O wait), disque, réseau, ...
- `%hi` et `%si` : Temps utilisé par les interruptions matérielles et logicielles.
- `%st` : Temps utilisé par un hyperviseur (en cas de virtualisation).

*Il est possible d'afficher la charge CPU par coeur, avec l'option `top 1`.*

---
# Utilisation de la mémoire

``` bash
MiB Mem :   1968.2 total,    232.7 free,    347.3 used,   1575.2 buff/cache     
MiB Swap:   2048.0 total,   2048.0 free,      0.0 used.   1621.0 avail Mem 
```

- **mémoire RAM** et **mémoire swap** :
  - **1968.2 total** : quantité de mémoire  totale,
  - **232.7 free** : quantité de mémoire  libre
  - **347.3 used** : quantité de mémoire  utilisée

---
#  "buff/cache" et "avail Mem"

``` bash
MiB Mem :   1968.2 total,    232.7 free,    347.3 used,   1575.2 buff/cache     
MiB Swap:   2048.0 total,   2048.0 free,      0.0 used.   1621.0 avail Mem 
```

- **1575.2 buff/cache** :
  - Mémoire tampon et cache utilisée par le noyau pour optimiser les performances d’accès aux données.  
  - "Libérable" si besoin, Mémoire réellement disponible : 232.7 free + 1575.2 buff/cache  = 1807.9 MiB
- **1621.0 avail Mem** :  
  - Estimation de mémoire RAM sans devoir recourir au swap. 
  - Si très basse : le système va devoir swapper.

---
# Détail de la liste des processus

``` bash
  PID USER  PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
    1 root  20   0   22560  13696   9600 S   0.0   0.7   0:07.54 systemd
```

- **PID** : Identifiant du processus.
- **USER** : Nom de l’utilisateur qui a lancé le processus.
- **PR** et **NI** : Priorité (PR) et Nice (NI) du processus. Nice détermine la priorité de l'ordonnancement du processus.
- **VIRT, RES, SHR** : Utilisation mémoire du processus.
  - **VIRT** : Mémoire virtuelle totale utilisée par le processus (y compris les fichiers mappés en mémoire).
  - **RES** : Mémoire résidente, ou physique, utilisée par le processus.
  - **SHR** : Mémoire partagée utilisée par le processus.

---

``` bash
  PID USER  PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
    1 root  20   0   22560  13696   9600 S   0.0   0.7   0:07.54 systemd
```
- **S** : État du processus : **R**unning, **S**leeping, **D**isk sleep (attend une I/O), s**T**opped, **Z**ombie.
- **%CPU** : Pourcentage de l’utilisation du CPU par le processus.
- **%MEM** : Pourcentage de l’utilisation de la mémoire RAM par le processus.
- **TIME+** : Temps total CPU utilisé par le processus depuis son lancement.
- **COMMAND** : Nom de la commande ayant lancé le processus.

