---
hide:
  - navigation
  - toc
---

# Linux - 102

Administration  Linux

---

Slides de la présentation:

- Version en ligne: [linux-102.html](linux-102.html)
- version PDF: [linux-102.pdf](linux-102.pdf)

---

Les labs correspondants à cette session sont disponibles en ligne : [https://ebraux.gitlab.io/linux-labs/](https://ebraux.gitlab.io/linux-labs/)


---

![image alt <>](assets/linux.png)




