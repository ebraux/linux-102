# QCM LINUX 101

---
Un fichier dispose des droits suivants -r--r----, quels sont les droits qui s'appliquent à un membre du groupe propriétaire sur ce fichier ?
- A : Il pourra lire et écrire le fichier.
- B : Il pourra seulement lire le fichier.
- C : Il n'aura aucun droit.
Réponse B

---
Que fais la commande cd ?
- A : Elle crée un répertoire.
- B : Elle permet de se déplacer dans un répertoire.
- C : Elle permet de changer les droits sur un répertoire.
Réponse B

---
Quelle commande  permet de savoir où se trouve un exécutable :
- A : witch
- B : which
- C : where
Réponse : B

---
Quel est le rôle de la variable d'environnement $PATH ?
- A : Elle définit les répertoires où le système cherche des exécutables.
- B : Elle définit les fichiers de configuration du système.
- C : Elle détermine le répertoire de l'utilisateur actuel.
Réponse : A

---
Quelle commande permet d'afficher le contenu de la variable $PATH ?
- A : echo $PATH
- B : path
- C : ls $PATH
Réponse : A

---
Quels répertoires sont typiquement inclus dans le PATH sous Linux ?
- A : /usr/bin, /bin, /usr/local/bin
- B : /home, /tmp, /var
- C : /usr/share, /opt, /dev
Réponse : A

---
Quel est le risque de modifier le PATH en y incluant des répertoires non sécurisés ?
- A : Cela augmente la vitesse d'exécution des programmes.
- B : Cela peut permettre à un attaquant de remplacer des commandes critiques.
- C : Cela n'affecte pas la sécurité du système.
Réponse : B



---
## SSH

---
Quelle commande permet de générer une paire de clés SSH ?
- A : ssh-keygen -t ed25519
- B : ssh-copy-id
- C :  ssh-add
Réponse : A

---
La clé privée doit être partagée avec le serveur pour une connexion SSH sécurisée.
Réponse : Faux (La clé privée doit rester secrète sur le client.)

---
Il est important de rendre la clé privée accessible uniquement à l'utilisateur propriétaire pour éviter les risques de sécurité.
Réponse : Vrai

---
Quelle commande permet de sécuriser la clé privée en limitant l'accès ?
- A : chmod 600 ~/.ssh/id_ed25519
- B : chmod 777 ~/.ssh/id_ed25519
- C : chmod 400 ~/.ssh/id_ed25519

---
Quel fichier contient la clé publique sur un serveur pour permettre l'authentification par clé ?
- A : /etc/ssh/authorized_keys
- B : ~/.ssh/id_rsa.pub
- C : ~/.ssh/authorized_keys
Réponse : C

---
SSH peut être utilisé pour exécuter des commandes sur une machine distante sans ouvrir une session interactive.
Réponse : Vrai


---
Quelle commande permet de lister les fichiers dans un répertoire distant via SSH ?
- A : ssh user@host ls
- B :  ssh user@host list
- C : ssh user@host -l
Réponse : A

---
Quel est le résultat de la commande ssh user@host pwd ?
- A : Affiche le répertoire de travail sur le serveur distant
- B : Affiche les fichiers du répertoire courant
- C : Affiche la liste des utilisateurs sur le serveur
Réponse : A


---
## Gestion des applications 

---
La commande apt update permet :
- A : de mettre à jour le système d'exploitation
- B : d'obtenir la liste des paquets à mettre à jour
- C : de mettre à jour la liste des paquets disponibles 
Réponse C

---
L'ajout d'une clef GPG lors de l'ajout d'une source de paquet permet :
- A : de déchiffrer les paquetages présent sur le dépôt
- B : de s'authentifier auprès du dépôt pour permettre le téléchargement des paquets
- C : d'instaurer une confiance envers les paquets du mainteneur du dépôt
Réponse C



---
## Gestion des utilisateurs

---
L'UID 1000 est le premier UID utilisable pour un utilisateur sous Linux.
Réponse : Vrai

---
L'UID 0 est réservé à l'utilisateur root.
Réponse : Vrai

---
L'UID de l'utilisateur root est :
- A : 1000
- B : 0
- C :  500
Réponse : B

---
Les GID inférieurs à 1000 sont réservés pour les groupes utilisateurs.
Réponse : Faux (Ils sont réservés pour les groupes système.)

---
Quelle commande permet d'afficher les informations sur l'UID et le GID d'un utilisateur ?
- A : ls
- B : id
- C : groups
Réponse : B

---
Quelle information est incluse dans la sortie de la commande id ?
- A : Seulement l'UID
- B : L'UID, le GID et les groupes de l'utilisateur
- C : Seulement le nom de l'utilisateur
Réponse : B

---
Quel fichier contient les informations des utilisateurs sous Linux ?
- A : /etc/shadow
- B : /etc/passwd
- C : /etc/group
Réponse : B

---
La commande `usermod -aG` permet d'ajouter un utilisateur à un groupe sans le retirer des groupes existants.
Réponse : Vrai

---
La commande `su` permet de se connecter sous un autre utilisateur sans quitter la session actuelle.
Réponse : Vrai

---
La commande `sudo` permet de changer d'utilisateur et d'exécuter des commandes avec des privilèges d'administrateur.
Réponse : Vrai

---
La commande `sudo` peut être utilisée sans mot de passe si la configuration` /etc/sudoers` est modifiée.
Réponse : Vrai

---
Que permet cette règle sudo `bob ALL = (ALL) NOPASSWD:/sbin/iptables`
- A : Permet à bob de lancer iptables sans mot de passe.
- B : Permet à tout les utilisateurs de lancer iptables en tant que bob.
- C : Permet au groupe bob de lancer iptables.
Réponse A

---
Quelle commande permet de devenir super utilisateur dans une session en cours sans changer d'utilisateur ?
- A :  sudo su
- B :  su
- C :  sudo -i
Réponse B  

---
Quelle commande permet d'exécuter une seule commande en tant que super-utilisateur sans ouvrir une session entière ?
- A : sudo
- B : su
- C :  chmod
Réponse : A

---
Quelle est la principale différence entre su et sudo ?
- A :  `su` change de session utilisateur, tandis que `sudo` permet d'exécuter une commande avec des privilèges.
- B : `su` nécessite un mot de passe root, tandis que `sudo` nécessite un mot de passe utilisateur.
- C: `su` permet d'exécuter plusieurs commandes à la fois.
Réponse : A


---
## Gestion des services 

---
Quelle commande utiliser pour redémarrer le service sshd 
- A : sudo systemctl restart sshd.service
- B : sudo sshd service restart
- C : sudo systemctl reload sshd.service
Réponse A

---
Quelle commande reconfigure un service sans le redémarrer ?
- A : sudo systemctl reload
- B : sudo systemctl restart
- C : sudo systemctl reboot

---
La commande `systemctl enable` redémarre un service en cours d'exécution.
Réponse : Faux

---
Que fait la commande systemctl list-units ?
- A : Arrête les unités inactives.
- B : Affiche les services actifs.
- C : Redémarre tous les services.
Réponse : B


---
Quels sont les types d'unité SystemD dans la liste ci dessous (choix multiple)
- A : path
- B : date
- C : time
- D : base
Réponse A et C

---
Quels fichiers de configuration sont rechargés par systemctl daemon-reload ?

- A : Les fichiers /etc/passwd
- B : Les fichiers d’unité .service
- C : Les fichiers /var/log
Réponse : B

---
Quelle commande désactive un service tout en laissant le processus actif ?
- A : systemctl stop
- B :  systemctl disable
- C :  systemctl mask
Réponse : B

---
Pour modifier un service, la bonne pratique consiste à modifier directement le fichier correspondant dans /lib/systemd/system/ 
Réponse : Faux

---
Que se passe-t-il si vous modifiez un fichier d’unité et oubliez de recharger SystemD ?
- A : Les modifications sont appliquées automatiquement.
- B : Les modifications ne sont pas prises en compte avant un redémarrage complet.
- C : Les modifications nécessitent un `systemctl daemon-reload`.
Réponse : C

---
Dans quel dossier est-il préférable de placer les fichiers de surcharge pour personnaliser un service ?
- A : /etc/systemd/system/<service>.d/
- B : /lib/systemd/system/
- C : /usr/share/systemd/
Réponse : A


---
Quel est l’avantage principal du mode texte lors du démarrage d'un système Linux avec SystemD ?
- A : Moins de ressources système utilisées
- B : Interface utilisateur plus avancée
- C : Meilleure compatibilité avec les anciennes machines
Réponse : A


---
## Gestion des performances

---
Quelle commande utiliser pour savoir quel processus surcharge ma machine : 
- A : top
- B : netstat
- C : journalctl
Réponse A

---
La valeur idéale du load average pour un processeur à 4 cœurs est 1.00.
Réponse : Faux (La valeur idéale pour un processeur à 4 cœurs est 4.00.)

---
Que signifie un load average inférieur à 1 ?
- A : Le processeur est surchargé
- B : Le processeur est sous-utilisé
- C : Le système est à l'arrêt

---
Que représente la valeur %CPU affichée dans top ?
- A : Le temps CPU utilisé par le système d'exploitation
- B : Le pourcentage de CPU utilisé par un processus
- C : Le pourcentage de mémoire utilisé
Réponse : B

---
Quelle option dans top permet d'afficher une ligne par cœur de processeur ?
- A : 1
- B : i
- C : m
Réponse : A